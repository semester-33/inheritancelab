package polymorphism;

public class BookStore {
    public static void main(String[] args){

        Book[] books = new Book[5];

        ElectronicBook a = (ElectronicBook)books[0];

        books[0] = new Book("A Day in the Life of Rida", "Reeda Grinch");
        books[1] = new ElectronicBook("How is wikihow ruining our limbs", "Michelle", 90000);
        books[2] = new Book("How to become Swetha", "Swetha");
        books[3] = new ElectronicBook("Gaming doesn't ruin lives, lives ruin gaming", "Rida", 42069);
        books[4] = new ElectronicBook("How to trick adults into babyhood", "Dr. Phill", 8000);
        
        for(int i = 0; i < books.length; i++){
            System.out.println(books[i]);
        }

        System.out.println(a.getNumberBytes());
    
    }
}
